﻿namespace Blog.Web.Api.ViewModels
{
    public class FacebookAuthViewModel
    {
        public string AccessToken { get; set; }
    }
}
