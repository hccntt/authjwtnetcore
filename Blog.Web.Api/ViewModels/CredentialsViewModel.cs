﻿using Blog.Web.Api.ViewModels.Validations;
using FluentValidation.Attributes;

namespace Blog.Web.Api.ViewModels
{
    [Validator(typeof(CredentialsViewModelValidator))]
    public class CredentialsViewModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
